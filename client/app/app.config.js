'use strict';

export function routeConfig($urlRouterProvider, $locationProvider) {
  'ngInject';

  $urlRouterProvider.otherwise('/');

  $locationProvider.html5Mode(true);
}

export function appConfig($translateProvider){
  'ngInject';

  $translateProvider.useMessageFormatInterpolation();
  $translateProvider.useStaticFilesLoader({
    prefix: 'app/common/languages/',
    suffix: '.json'
  });

  $translateProvider.preferredLanguage('de');
}
