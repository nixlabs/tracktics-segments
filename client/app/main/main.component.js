import angular from 'angular';
import uiRouter from 'angular-ui-router';
import routing from './main.routes';

export class MainController {

  statInfoSingleComparison = {
    title: "DISTANCE COVERED",
    subtitle: "In Selected Period",
    data: "11.2",
    additionalData: "km",
    class: "glyphicon glyphicon-star"
  };
  shapeSingleStatInfo1 = {
    title: "DISTANCE",
    subtitle: "20% less distance than average",
    class: "glyphicon glyphicon-arrow-down"
  };
  shapeSingleStatInfo2 = {
    title: "Sprint",
    subtitle: "30% more than sprints than team average",
    class: "glyphicon glyphicon-arrow-up"
  };
  shapeSingleStatInfo3 = {
    title: "Team",
    subtitle: "Player with best overall performances this week",
    class: "glyphicon glyphicon-arrow-down"
  };
  sessionMetaDataInfo = {
    title: "Session Info",
    data: [
      {name: "Time & Date", additionalData: "13:43 - 15:21 / Sat, 21 Feb 2017"},
      {name: "Playground", additionalData: "Stamford Bridge"},
      {name: "City", additionalData: "London, United Kingdom"},
      {name: "Players", additionalData: "11 players / 2 sustitutions"},
      {name: "Weather", additionalData: "8 C / No Wind"},
      {name: "Pitch Condition", additionalData: "Exellent"}
    ]
  };
  sessionResultInfo = {
    home: {
      img: "http://icons.iconarchive.com/icons/giannis-zographos/english-football-club/128/Chelsea-FC-icon.png",
      name: "Chelsea",
      goals: "2"
    },
    away: {
      img: "http://assets2.lfcimages.com/uploads/teams/manu_120X120.png",
      name: "Manchester United",
      goals: "1"
    }
  };
  statInfoDoubleComparisonPercentageInfo = {
    info1: {
      title: "Total Distance Covered",
      subtitle: "",
      data: "10.4",
      additionalData: "km",
      percentage: "+8%",
      class: "glyphicon glyphicon-arrow-up"
    },
    info2: {
      title: "Total Distance in Highspeed Zones",
      subtitle: "",
      data: "1.7",
      additionalData: "km",
      percentage: "+8%",
      class: "glyphicon glyphicon-arrow-up"
    }
  };
  buttonGroupSelectorData = {
    title: "test",
    class: "testclass",
    btndata: [
      {
        title: "All Sprints",
        selected: true,
        callback: this.buttonGroupSelectorCallBack
      },
      {
        title: "Progresive Sprints",
        selected: false,
        callback: this.buttonGroupSelectorCallBack
      },
      {
        title: "Offensive Sprints",
        selected: false,
        callback: this.buttonGroupSelectorCallBack
      },
      {
        title: "Defensive Sprints",
        selected: false,
        callback: this.buttonGroupSelectorCallBack
      }]
  };
  showEditableInput = true;

  team = {
    teamLogo: "Igor",
    teamName: "Nikolov",
    homeCity: "Skopje",
    homeGround: "Nix",
  };
  editableInputData = {
    image: {url: this.team.teamLogo},
    title: [{name: this.team.teamName}],
    info: [
      {title: this.team.homeCity, subtitle: "Home City"},
      {title: this.team.homeGround, subtitle: "Homeground"}
    ]
  };
  testEditableInput = {
    name: "Tracktics"
  };
  editableInputData1 = {
    title: {name: this.testEditableInput.name}
  };

  testEditableEmailInput = {
    name: "ini@nix.mk"
  };
  editableEmailData1 = {
    title: {name: this.testEditableEmailInput.name}
  };

  chartConfig = {

    plotOptions: {
      series: {
        fillOpacity: 1
      }
    },

    series: [{
      name: '1',
      data: [129.2, 144.0, 148.5, 216.4, 194.1, 95.6, 54.4, null, null, null, null, null, null, null],
      type: 'area',
      color: 'green'
    }, {
      name: '2',
      data: [null, null, null, null, null, null, 54.4, 129.2, 144.0, 148.5, 216.4, 194.1, 95.6, 54.4],
      type: 'area',
      color: 'transparent'
    },
      {
        name: '3',
        data: [null, null, null, null, null, null, null, null, null, null, null, null, null, 54.4, 129.2, 144.0, 148.5, 216.4, 194.1, 95.6, 54.4],
        type: 'area',
        color: 'red'
      }]

  };





  /*@ngInject*/
  constructor($http) {
    this.$http = $http;
  }


  $onInit() {
    this.$http.get('/api/things')
      .then(response => {
        this.awesomeThings = response.data;
      });
  }

  buttonGroupSelectorCallBack() {
    console.log("this is buttonGroupSelectorCallBack");
  }
  highcharts = Highcharts;
  chartOptions = {
    'chartDonultTitle': null,
    'chartDonultPercent': null,
    'chartDonultValue': null,
    'DonultStyle': null
  };

  Test() {
    var vm = this;
    this.highcharts.chart('chart2', {
      chart: {

        events: {
          load: function (event,a,b,c) {
            event.target.reflow();
            var chart = event.target;
            var offsetTop = angular.element(document.getElementById('pieChartInfoText'))[0].offsetTop;
            var textX = chart.plotLeft + (chart.plotWidth * 0.5);
            var textY = chart.plotTop + (chart.plotHeight * 0.5);
            vm.chartOptions.chartDonultTitle = "Highsped running";
            vm.chartOptions.chartDonultPercent = "10%";
            vm.chartOptions.chartDonultValue = "1.1km";
            vm.chartOptions.DonultStyle = {
              "left": textX + (68 * -0.5) +"px",
              "top": textY + (91 * -0.5) + "px"
            }
          }
        },
        renderTo: 'container',
        type: 'pie'
      },
      plotOptions: {
        pie: {
          innerSize: '90%'
        }
      },
      series: [{
        data: [
          ['Firefox', 44.2],
          ['IE7', 26.6],
          ['IE6', 20],
          ['Chrome', 3.1],
          ['Other', 5.4]
        ]
      }]
    });
  }


  columnStacked() {
    Highcharts.chart('columnStacked', {
      chart: {
        type: 'column',
      },
      title: {
        text: 'Stacked column chart'
      },
      xAxis: {
        categories: ['Apples', 'Oranges', 'Pears', 'Grapes', 'Bananas']
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Total fruit consumption'
        },
        stackLabels: {
          enabled: true,
          style: {
            fontWeight: 'bold',
            color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
          }
        }
      },
      legend: {
        align: 'right',
        x: -30,
        verticalAlign: 'top',
        y: 25,
        floating: true,
        backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
        borderColor: '#CCC',
        borderWidth: 1,
        shadow: false
      },
      tooltip: {
        headerFormat: '<b>{point.x}</b><br/>',
        pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}',
        useHTML: true,
        formatter:function () {
          return '<div class="panel panel-default"> <div class="panel-heading">Exclude from Analysis</div> <div class="panel-body"><i class="fa fa-trophy" aria-hidden="true"></i> Match vs Liverpool </div> <div class="panel-footer">Session Details</div> </div>'
        }
      },
      plotOptions: {
        column: {
          stacking: 'normal',
          dataLabels: {
            enabled: true,
            color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
          }
        },
        series: {
          cursor: 'pointer',
          point: {
            events: {
              click: function (event) {
                alert(
                  this.name + ' clicked\n' +
                  'Alt: ' + event.altKey + '\n' +
                  'Control: ' + event.ctrlKey + '\n' +
                  'Meta: ' + event.metaKey + '\n' +
                  'Shift: ' + event.shiftKey
                );
              }
            }
          }
        }

      },
      series: [{
        name: 'John',
        data: [5, 3, 4, 7, 2]
      }, {
        name: 'Jane',
        data: [2, 2, 3, 2, 1]
      }, {
        name: 'Joe',
        data: [3, 4, 4, 2, 5]
      }]
    });
  }

  type = {
    offensiveAndDefensive: [
      {class: "top"},
      {class: "center"},
      {class: "bottom"}
  ],
  sideDistribution:[
    {class: "left"},
    {class: "middle"},
    {class: "right"}
  ]}

}


let Main = {
  bindings: {},
  template: require('./main.html'),
  controllerAs: 'vm',
  controller: MainController
};

export default angular.module('trackticsSegmentsApp.main', [uiRouter])
  .config(routing)
  .component('main', Main)
  .name;
