class SessionPeriodSelectorController {

    /*@ngInject*/
    constructor() {
    }

    $onInit() {
    }
}

let SessionPeriodSelector = {
    template: require('./sessionPeriodSelector.html'),
    controller: SessionPeriodSelectorController,
    controllerAs: 'vm'
};

export default SessionPeriodSelector;
