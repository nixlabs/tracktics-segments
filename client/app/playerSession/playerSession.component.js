import angular from 'angular';
import PlayerCardInfo from './playerCardInfo.component';
import ShapeStatsNotification from './shapeStatsNotification.component';
import SessionDetails from './sessionDetails.component';
import SessionPeriodSelector from './sessionPeriodSelector.component';
import SessionActivityGraph from './sessionActivityGraph.component';
import SessionSpeedZones from './sessionSpeedZones.component';
import SessionTactical from './sessionTactical.component';
import SessionDistance from './sessionDistance.component';
import SessionSprints from './sessionSprints.component';
import routing from '../components.routes';

class PlayerSessionController {
  statInfoSingleComparison = {
    title:"DISTANCE COVERED",
    subtitle:"In Selected Period",
    data: "11.2",
    additionalData: "km",
    class: "glyphicon glyphicon-star"
  };
    /*@ngInject*/
    constructor() {
    }

    $onInit() {
    }
}

let PlayerSession = {
    template: require('./playerSession.html'),
    controller: PlayerSessionController,
    controllerAs: 'vm'
};

export default angular.module('trackticsSegmentsApp.playersession', [])
    // .config(routing)
    .component('playerSession', PlayerSession)
    .component('playerCardInfo', PlayerCardInfo)
    .component('shapeStatsNotification', ShapeStatsNotification)
    .component('sessionDetails', SessionDetails)
    .component('sessionPeriodSelector', SessionPeriodSelector)
    .component('sessionActivityGraph', SessionActivityGraph)
    .component('sessionSpeedZones', SessionSpeedZones)
    .component('sessionTactical', SessionTactical)
    .component('sessionDistance', SessionDistance)
    .component('sessionSprints', SessionSprints)
    .name;
