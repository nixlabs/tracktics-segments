class SessionActivityGraphController {

    /*@ngInject*/
    constructor() {
    }

    $onInit() {
    }
}

let SessionActivityGraph = {
    template: require('./sessionActivityGraph.html'),
    controller: SessionActivityGraphController,
    controllerAs: 'vm'
};

export default SessionActivityGraph;
