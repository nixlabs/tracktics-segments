class PlayerCardInfoController {
  statInfoSingleComparison = {
    title: "DISTANCE COVERED",
    subtitle: "In Selected Period",
    data: "11.2",
    additionalData: "km",
    class: "glyphicon glyphicon-star"
  };
    /*@ngInject*/
    constructor() {
    }

    $onInit() {
    }
}

let PlayerCardInfo = {
    template: require('./playerCardInfo.html'),
    controller: PlayerCardInfoController,
    controllerAs: 'vm'
};

export default PlayerCardInfo;
