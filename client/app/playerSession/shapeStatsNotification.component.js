class ShapeStatsNotificationController {

    /*@ngInject*/
    constructor() {
    }

    $onInit() {
    }
}

let ShapeStatsNotification = {
    template: require('./shapeStatsNotification.html'),
    controller: ShapeStatsNotificationController,
    controllerAs: 'vm'
};

export default ShapeStatsNotification;
