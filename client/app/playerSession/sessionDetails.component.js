class SessionDetailsController {
  statInfoSingleComparison = {
    title: "DISTANCE COVERED",
    subtitle: "In Selected Period",
    data: "11.2",
    additionalData: "km",
    class: "glyphicon glyphicon-star"
  };
    /*@ngInject*/
    constructor() {
    }

    $onInit() {
    }
}

let SessionDetails = {
    template: require('./sessionDetails.html'),
    controller: SessionDetailsController,
    controllerAs: 'vm'
};

export default SessionDetails;
