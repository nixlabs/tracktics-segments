export class shapeSingleStatAppController {
  notifications = {
    first: {
      title: 'physicalHighlights',
      subtitle: '20% less distance covered than average',
      class: 'down'
    },
    second: {
      title: 'tacticalHighlights',
      subtitle: '30% more sprints than team average',
      class: 'up'
    },
    third: {
      title: 'mentalHighlights',
      subtitle: 'Player with best overall performance this week',
      class: 'down'
    }
  };
  showJSONeditor = false;
  obj = {data:this.notifications, options: {mode: 'tree'}}
  newShapeSingleStatInfo1 = null;
  /*@ngInject*/
  constructor() {
  }

  $onInit() {

  }
  btnOnClick(){
    this.showJSONeditor = !this.showJSONeditor;
    this.newShapeSingleStatInfo1 = this.notifications;
    this.obj = {data:this.newShapeSingleStatInfo1, options: {mode: 'tree'}}
  }
  changeOptions () {
    this.obj.options.mode = this.obj.options.mode == 'tree' ? 'code' : 'tree';
  };
  showJSON(obj) {
    return angular.toJson(obj, true);
  };
  btnSave(){
    this.notifications = this.obj.data;

  }
}

let shapeSingleStatApp = {
  bindings: {
    data: '<'
  },
  template: require('./shapeSingleStat.html'),
  controllerAs: 'vm',
  controller: shapeSingleStatAppController
};

export default shapeSingleStatApp;
