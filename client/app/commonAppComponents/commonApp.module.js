import angular from 'angular';
// import routing from '../components.routes';
import StatInfoSingleComparisonApp from './statInfoSingleComparison/statInfoSingleComparison.component';
import shapeSingleStatApp from  './shapeSingleStat/shapeSingleStat.component'
import sessionMetaDataApp from './sessionMetadata/sessionMetadata.component'
import sessionResultApp from './sessionResult/sessionResult.component'
import arrowApp from './arrow/arrow.component'
import buttonGroupSelectorApp from './buttonGroupSelector/buttonGroupSelector.component'
import editableEmailInputApp from './editableEmailInput/editableEmailInput.component'
import editableInputApp from './editableInput/editableInput.component'

import periodSelectorApp from './periodSelector/periodSelector.component'
import statInfoDonutApp from './statInfoDonut/statInfoDonut.component'
import playerDetailsApp from './playerDetails/playerDetails.component'
import pitchZoneApp from './pitchZone/pitchZone.component'
import areaSplineApp from './graphs/area/areaSpline.component'
import barGraphApp from './graphs/bar/bar.component'
import doubleDataComparisonApp from './doubleDataComparison/doubleDataComparison.component'
import buttonIconApp from './buttonIcon/buttonIcon.component'

export default angular.module('trackticsSegmentsApp.commonApp', [])
  // .config(routing)
  .component('statInfoSingleComparisonApp', StatInfoSingleComparisonApp)
  .component('shapeSingleStatApp', shapeSingleStatApp)
  .component('sessionMetaDataApp', sessionMetaDataApp)
  .component('sessionResultApp', sessionResultApp)
  .component('arrowApp', arrowApp)
  .component('buttonGroupSelectorApp', buttonGroupSelectorApp)
  .component('editableInputApp', editableInputApp)
  .component('editableEmailInputApp', editableEmailInputApp)
  .component('periodSelectorApp', periodSelectorApp)
  .component('statInfoDonutApp', statInfoDonutApp)
  .component('playerDetailsApp', playerDetailsApp)
  .component('pitchZoneApp', pitchZoneApp)
  .component('areaSplineApp', areaSplineApp)
  .component('barGraphApp', barGraphApp)
  .component('doubleDataComparisonApp', doubleDataComparisonApp)
  .component('buttonIconApp', buttonIconApp)

  .name;
