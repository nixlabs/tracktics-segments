class AreaSplineAppController {


  showJSONeditor = false;
  obj = {data:this.metrics, options: {mode: 'tree'}}
  newArrowData = null;
  /*@ngInject*/
  constructor($element) {
    this.metrics = {"data":[0.33, 1.49, 1.44, 1.9, 5.41, 6.89, 8.46, 6.92, 5.22, 6.12, 5.45, 4.97, 6.53, 7.67, 9.86, 7.94, 9.93, 7.95, 7.25, 6.02, 7.11, 7.24, 6.51, 7.41, 3.79, 2.88, 5.6, 4.4, 5.57, 6.69, 5.99, 4.73, 6.88, 4.9, 5.41, 2.66, 1.22, 1.7, 1.47, 6.82, 5.27, 7.46, 6.18, 6.55, 6.99, 5.29, 5.11, 5.03, 6.48, 5.44, 6.15, 6.86, 8.2, 6.39, 7.09, 4.57, 3.48, 3.12, 1.66, 1.09, 1.62, 7.31, 6.82, 6.23, 6.61, 5.91, 5.73, 6.78, 4.85, 5.71, 5.43, 6.04, 6.56, 5.36, 5.27, 5, 5.95, 7.74, 5.95, 5.17, 4.75, 5.23, 4.25, 7.01, 6.86, 6.97, 5.21, 6.52, 5.12, 0],
      "labels": [0, 3, 6, 9, 12, 15, 18, 21, 24, 27, 30, 33, 36, 39, 42, 45, 48, 51, 54, 57, 60, 63, 66, 69, 72, 75, 78, 81, 84, 87, 90, 93, 96, 99, 102, 105, 108, 111, 114, 117, 120, 123, 126, 129, 132, 135, 138, 141, 144, 147],
      "series":["speed"]};
  }
  $onInit() {

  }
  $onChanges(obj) {

  }

  btnOnClick(){
    this.showJSONeditor = !this.showJSONeditor;
    this.newArrowData = this.metrics;
    this.obj = {data:this.newArrowData, options: {mode: 'tree'}}
  }
  changeOptions () {
    this.obj.options.mode = this.obj.options.mode == 'tree' ? 'code' : 'tree';
  };
  showJSON(obj) {
    return angular.toJson(obj, true);
  };
  btnSave(){
    this.metrics = this.obj.data;

  }


}

let AreaSplineApp = {
  bindings: {},
  template: require('./areaSpline.html'),
  controllerAs: 'vm',
  controller: AreaSplineAppController
};

export default AreaSplineApp
