class BarGraphAppController {
  distanceName = "distance";
  compareDistanceName = "compareDistance";

  data = [1,2,3,4,5,6,7,8,9,1,3,5,7,8,9,3,2,34,5,3,4,5,6,7,8,96,5,4,3,23];
  labels = [1,2,3,4,5,6,7,8,9,1,3,5,7,8,9,3,2,34,5,3,4,5,6,7,8,-5,5,4,3,23];
  data1 = [1,2,3,4,-5,6,7,8,9,1,3,5,7,8,-9,3,2,-34,5,3,4,-5,6,7,8,-30,5,4,3,-23];

  distance = {data:this.data,labels:this.labels};
  compareDistance ={data:this.data1,labels:this.labels};

  showDistanceJSONeditor = false;
  showCompareDistanceJSONeditor = false;
  objDistance = {data:this.distance, options: {mode: 'tree'}};
  objCompareDistance = {data:this.compareDistance, options: {mode: 'tree'}};
  newArrowData = null;
  newCompareDistance = null;
  /*@ngInject*/
  constructor() {
  }

  $onInit() {

  }
  btnOnClick(name){
    if(name == this.distanceName) {
      this.showDistanceJSONeditor = !this.showDistanceJSONeditor;
      this.newArrowData = this.distance;
      this.objDistance = {data:this.newArrowData, options: {mode: 'tree'}};
    }
    else if(name == this.compareDistanceName){
      this.showCompareDistanceJSONeditor = !this.showCompareDistanceJSONeditor;
      this.newCompareDistance = this.compareDistance;
      this.objCompareDistance = {data:this.newCompareDistance, options: {mode: 'tree'}}
    }

  }
  changeOptions (name) {
    if(name == this.distanceName) {
      this.objDistance.options.mode = this.objDistance.options.mode == 'tree' ? 'code' : 'tree';
    }
    else if(name == this.compareDistanceName) {
      this.objCompareDistance.options.mode = this.objCompareDistance.options.mode == 'tree' ? 'code' : 'tree';
    }
  };
  showJSON(obj) {
    return angular.toJson(obj, true);
  };
  btnSave(name){
    if(name == this.distanceName) {
      this.distance = this.objDistance.data;
    }
    else if(name == this.compareDistanceName) {
      this.compareDistance = this.objCompareDistance.data;
    }

  }
}

let BarGraphApp = {
  bindings: {},
  template: require('./barGraph.html'),
  controllerAs: 'vm',
  controller: BarGraphAppController
};

export default BarGraphApp
