class DoubleDataComparisonAppController{

  playerDistributions = [[12,55,33],[51,34,15],[null,null]];
  teamDistributions= {
    "bottom":"17.98",
    "center":"46.59",
    "top":"35.42",
    "left":"12.96",
    "middle":"52.94",
    "right":"34.11"
  };
  playerTeamDistributions = {playerDistributions:this.playerDistributions,teamDistributions:this.teamDistributions};

  showJSONeditor = false;
  obj = {data:this.playerTeamDistributions, options: {mode: 'tree'}}
  newArrowData = null;
  /*@ngInject*/
  constructor() {
  }

  $onInit() {

  }
  btnOnClick(){
    this.showJSONeditor = !this.showJSONeditor;
    this.newArrowData = this.playerTeamDistributions;
    this.obj = {data:this.newArrowData, options: {mode: 'tree'}}
  }
  changeOptions () {
    this.obj.options.mode = this.obj.options.mode == 'tree' ? 'code' : 'tree';
  };
  showJSON(obj) {
    return angular.toJson(obj, true);
  };
  btnSave(){
    this.playerTeamDistributions = this.obj.data;

  }
}

let DoubleDataComparisonApp = {
    bindings: {},
    template: require('./doubleDataComparison.html'),
    controllerAs: 'vm',
    controller: DoubleDataComparisonAppController
};

export default DoubleDataComparisonApp
