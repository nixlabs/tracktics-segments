/**
 * Created by Igor on 08-Mar-17.
 */

export class editableEmailInputAppController {

  editButton = false;
  showEditableInput = true;

  editableInputEmailData = {
    name:"ini@nix.mk"
  };
  editableInputEmail = {
    title:{name: this.editableInputEmailData.name}
  };
  validationMsg = "Wrong Email";
  validate = true;

  showJSONeditor = false;
  obj = {data:this.editableInputEmail, options: {mode: 'tree'}}
  newArrowData = null;
  /*@ngInject*/
  constructor() {
  }

  $onInit() {

  }
  btnOnClick(){
    this.showJSONeditor = !this.showJSONeditor;
    this.newArrowData = this.editableInputEmail;
    this.obj = {data:this.newArrowData, options: {mode: 'tree'}}
  }
  changeOptions () {
    this.obj.options.mode = this.obj.options.mode == 'tree' ? 'code' : 'tree';
  };
  showJSON(obj) {
    return angular.toJson(obj, true);
  };
  btnSave(){
    this.editableInputEmail = this.obj.data;

  }

}

let editableEmailInputApp = {
    bindings: {},
    template: require('./editableEmailInput.html'),
    controllerAs: 'vm',
    controller: editableEmailInputAppController
};

export default editableEmailInputApp;
