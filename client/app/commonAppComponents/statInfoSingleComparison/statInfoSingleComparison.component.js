export class statInfoSingleComparisonAppController {
  stats = {
    totalDistance: {
      title: 'totalSessionDistance',
      data: 1000 / 1000,
      additionalData: 'km'
    },
    averageSessionPace: {
      title: 'averageSessionPace',
      data: 1000,
      additionalData: 'min/km'
    },
    selectedTotalDistance: {
      title: 'selectedPeriodDistance',
      data: 1000 / 1000,
      additionalData: 'km'
    },
    selectedAveragePace: {
      title: 'selectedPeriodAveragePace',
      data: 1000,
      additionalData: 'min/km'
    }
  };

  showJSONeditor = false;
  obj = {data:this.stats, options: {mode: 'tree'}}
  newStatInfoSingleComparison = null;
  /*@ngInject*/
  constructor() {
  }

  $onInit() {

  }
  btnOnClick(){
    this.showJSONeditor = !this.showJSONeditor;
    this.newStatInfoSingleComparison = this.stats;
    this.obj = {data:this.newStatInfoSingleComparison, options: {mode: 'tree'}}
  }
  changeOptions () {
    this.obj.options.mode = this.obj.options.mode == 'tree' ? 'code' : 'tree';
  };
  showJSON(obj) {
    return angular.toJson(obj, true);
  };
  btnSave(){
    this.stats = this.obj.data;

  }
}

let statInfoSingleComparisonApp = {
  bindings: {},
  template: require('./statInfoSingleComparison.html'),
  controllerAs: 'vm',
  controller: statInfoSingleComparisonAppController
};

export default statInfoSingleComparisonApp;
