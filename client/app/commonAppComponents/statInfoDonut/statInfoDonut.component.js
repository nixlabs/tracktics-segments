class StatInfoDonutAppController {
  speedZonesMetrics={
    walking: {
      label: {
        title: 'walking',
        value: (20 / 90 ) * 100,
        additionalValue: 200 / 1000,
        metric: 'km'
      },
      data: {
        data: [2000 / 2 , 1 - 10 / 100 ],
        labels: ['zone', 'rest']
      }
    },
    running: {
      label: {
        title: 'running',
        value: (20 / 90 ) * 100,
        additionalValue: 200 / 1000,
        metric: 'km'
      },
      data: {
        data: [2000 / 2 , 1 - 10 / 100 ],
        labels: ['zone', 'rest']
      }
    },
    highspeedRunning: {
      label: {
        title: 'highspeedRunning',
        value: (20 / 90 ) * 100,
        additionalValue: 200 / 1000,
        metric: 'km'
      },
      data: {
        data: [2000 / 2 , 1 - 10 / 100 ],
        labels: ['zone', 'rest']
      }
    },
    sprinting: {
      label: {
        title: 'sprinting',
        value: (20 / 90 ) * 100,
        additionalValue: 200 / 1000,
        metric: 'km'
      },
      data: {
        data: [2000 / 2 , 1 - 10 / 100 ],
        labels: ['zone', 'rest']
      }
    }
  };
  showJSONeditor = false;
  obj = {data:this.speedZonesMetrics, options: {mode: 'tree'}}
  newArrowData = null;
  /*@ngInject*/
  constructor() {
  }

  $onInit() {}
  $onChanges(obj) {}

  btnOnClick(){
    this.showJSONeditor = !this.showJSONeditor;
    this.newArrowData = this.speedZonesMetrics;
    this.obj = {data:this.newArrowData, options: {mode: 'tree'}}
  }
  changeOptions () {
    this.obj.options.mode = this.obj.options.mode == 'tree' ? 'code' : 'tree';
  };
  showJSON(obj) {
    return angular.toJson(obj, true);
  };
  btnSave(){
    this.speedZonesMetrics = this.obj.data;

  }

}

let StatInfoDonutApp = {
  bindings: {},
  template: require('./statInfoDonut.html'),
  controllerAs: 'vm',
  controller: StatInfoDonutAppController
};

export default StatInfoDonutApp
