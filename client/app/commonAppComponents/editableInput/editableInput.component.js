/**
 * Created by Igor on 08-Mar-17.
 */

export class editableInputAppController {

  editButton = false;
  isArray = false;
  newData = [];
  newStringData = null;

  showEditableInput = true;

  team = {
    teamLogo:"http://www.iconarchive.com/download/i12608/giannis-zographos/british-football-club/Chelsea.ico",
    teamName:"Chelsea",
    homeCity:"London,United Kingdom",
    homeGround:"Stamford Bridge",
  };
  teamEditableInput = {
    image: {url: this.team.teamLogo},
    title: [{name: this.team.teamName}],
    info: [
      {title: this.team.homeCity, subtitle: "Home City"},
      {title: this.team.homeGround, subtitle: "Homeground"}
    ]
  };
  player = {
    playerName:"Cesar",
    playerSurName:"Azpiliqueta",

  };

  playerInputData = {
    image: {url: this.team.teamLogo},
    title: [
      {name: this.player.playerName},
      {name: this.player.playerSurName}
    ],
    info: [
      {
        icon: "fa fa-pencil",
        title: "Trainings",
        subtitle: "Last training on 21.02.2017",
        additionalData: "48"
      },
      {
        icon: "fa fa-pencil",
        title: "Matches",
        subtitle: "Last match vs. Barnaley on 12.02.2017",
        additionalData: "27"
      }
      ]
  };
  testEditableInput = {
    name:"Tracktics"
  };
  editableInputData1 = {
    title:{name: this.testEditableInput.name}
  };

  showJSONeditor = false;
  obj = {data:this.playerInputData, options: {mode: 'tree'}}
  newArrowData = null;
  /*@ngInject*/
  constructor() {
  }

  $onInit() {

  }
  btnOnClick(){
    this.showJSONeditor = !this.showJSONeditor;
    this.newArrowData = this.playerInputData;
    this.obj = {data:this.newArrowData, options: {mode: 'tree'}}
  }
  changeOptions () {
    this.obj.options.mode = this.obj.options.mode == 'tree' ? 'code' : 'tree';
  };
  showJSON(obj) {
    return angular.toJson(obj, true);
  };
  btnSave(){
    this.playerInputData = this.obj.data;

  }
}



let editableInputApp = {
    bindings: {},
    template: require('./editableInput.html'),
    controllerAs: 'vm',
    controller: editableInputAppController
};

export default editableInputApp;
