class ButtonIconAppController{
  buttons = [{
    icon: 'backToTeam',
    text: 'backToTeam'
  },{
    icon: 'activity',
    text: 'comparePlayers'
  }];

  showJSONeditor = false;
  obj = {data:this.buttons, options: {mode: 'tree'}}
  newArrowData = null;
  /*@ngInject*/
  constructor() {
  }

  $onInit() {

  }
  btnOnClick(){
    this.showJSONeditor = !this.showJSONeditor;
    this.newArrowData = this.buttons;
    this.obj = {data:this.newArrowData, options: {mode: 'tree'}}
  }
  changeOptions () {
    this.obj.options.mode = this.obj.options.mode == 'tree' ? 'code' : 'tree';
  };
  showJSON(obj) {
    return angular.toJson(obj, true);
  };
  btnSave(){
    this.buttons = this.obj.data;

  }
}

let ButtonIconApp = {
    bindings: {},
    template: require('./buttonIcon.html'),
    controllerAs: 'vm',
    controller: ButtonIconAppController
};

export default ButtonIconApp
