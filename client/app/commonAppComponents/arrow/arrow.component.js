export class arrowAppController {

  arrowData = {
    data:"9",
    class: "small inner"
  };
  showJSONeditor = false;
  obj = {data:this.arrowData, options: {mode: 'tree'}}
  newArrowData = null;
  /*@ngInject*/
  constructor() {
  }

  $onInit() {

  }
  btnOnClick(){
    this.showJSONeditor = !this.showJSONeditor;
    this.newArrowData = this.arrowData;
    this.obj = {data:this.newArrowData, options: {mode: 'tree'}}
  }
  changeOptions () {
    this.obj.options.mode = this.obj.options.mode == 'tree' ? 'code' : 'tree';
  };
  showJSON(obj) {
    return angular.toJson(obj, true);
  };
  btnSave(){
    this.arrowData = this.obj.data;

  }
}

let arrowApp = {
  bindings: {
    data: '<'
  },
  template: require('./arrow.html'),
  controllerAs: 'vm',
  controller: arrowAppController
};

export default arrowApp;
