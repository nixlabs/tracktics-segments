export class sessionMetaDataAppController {

  /*SessionMetadata = {
    title: 'sessionInfo',
    start: timeUtils.getTimeFromDate(metadata[0].StartDate),
    end: timeUtils.getTimeFromDate(metadata[0].EndDate),
    date: timeUtils.getDateFromDateInFormat(metadata[0].StartDate, 'ddd, DD MMM YYYY'),
    pitchName: metadata.sportsField,
    city: metadata.geocode,
    playersCount: metadata.Sessions,
    substitutions: metadata.substitution.length,
    temperature: metadata.temperature,
    weather: "Sun",
    pitchCondition: "Test"
  };*/
  SessionMetadata = {
   title: 'sessionInfo',
   start: "20:00",
   end: "22:00",
   date: "11.12.2016",
   pitchName: "Gevgelija",
   city: "Gevgelija",
   playersCount: "11",
   substitutions: "4",
   temperature: "19",
   weather: "Sun",
   pitchCondition: "Test"
   };


  showJSONeditor = false;
  obj = {data:this.SessionMetadata, options: {mode: 'tree'}}
  newSessionMetaDataInfo = null;
  /*@ngInject*/
  constructor() {
  }

  $onInit() {

  }
  btnOnClick(){
    this.showJSONeditor = !this.showJSONeditor;
    this.newSessionMetaDataInfo = this.SessionMetadata;
    this.obj = {data:this.newSessionMetaDataInfo, options: {mode: 'tree'}}
  }
  changeOptions () {
    this.obj.options.mode = this.obj.options.mode == 'tree' ? 'code' : 'tree';
  };
  showJSON(obj) {
    return angular.toJson(obj, true);
  };
  btnSave(){
    this.SessionMetadata = this.obj.data;

  }
}

let sessionMetaDataApp = {
  bindings: {
    data: '<'
  },
  template: require('./sessionMetadata.html'),
  controllerAs: 'vm',
  controller: sessionMetaDataAppController
};

export default sessionMetaDataApp;
