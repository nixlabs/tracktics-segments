export class pitchZoneAppController {
  type = {
    offensiveAndDefensive: [
      {class: "top"},
      {class: "center"},
      {class: "bottom"}
    ],
    sideDistribution:[
      {class: "left"},
      {class: "middle"},
      {class: "right"}
    ]};

  showJSONeditor = false;
  obj = {data:this.type, options: {mode: 'tree'}}
  newArrowData = null;
  /*@ngInject*/
  constructor() {
  }

  $onInit() {

  }
  btnOnClick(){
    this.showJSONeditor = !this.showJSONeditor;
    this.newArrowData = this.type;
    this.obj = {data:this.newArrowData, options: {mode: 'tree'}}
  }
  changeOptions () {
    this.obj.options.mode = this.obj.options.mode == 'tree' ? 'code' : 'tree';
  };
  showJSON(obj) {
    return angular.toJson(obj, true);
  };
  btnSave(){
    this.type = this.obj.data;

  }
}

let pitchZoneApp = {
    bindings: {},
    template: require('./pitchZone.html'),
    controllerAs: 'vm',
    controller: pitchZoneAppController
};

export default pitchZoneApp;
