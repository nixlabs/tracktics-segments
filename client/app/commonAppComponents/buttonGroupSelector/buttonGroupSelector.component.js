export class buttonGroupSelectorAppController {

  distanceFilters = {
    title: 'averageInTimeslot',
    buttons: [
      {
        title:"oneMin",
        selected: false,
        data: "filterByOneMinute"
      },
      {
        title:"threeMin",
        selected: true,
        data: "filterByThreeMinutes"
      },
      {
        title:"fiveMin",
        selected: false,
        data: "filterByFiveMinutes"
      }]
  };
  showJSONeditor = false;
  obj = {data:this.distanceFilters, options: {mode: 'tree'}};
  newArrowData = null;
  /*@ngInject*/
  constructor() {
  }

  $onInit() {

  }
  btnOnClick(){
    this.showJSONeditor = !this.showJSONeditor;
    this.newArrowData = this.distanceFilters;
    this.obj = {data:this.newArrowData, options: {mode: 'tree'}}
  }
  changeOptions () {
    this.obj.options.mode = this.obj.options.mode == 'tree' ? 'code' : 'tree';
  };
  showJSON(obj) {
    return angular.toJson(obj, true);
  };
  btnSave(){
    this.distanceFilters = this.obj.data;

  }
}

let buttonGroupSelectorApp = {
  bindings: {
    data: '<'
  },
  template: require('./buttonGroupSelector.html'),
  controllerAs: 'vm',
  controller: buttonGroupSelectorAppController
};

export default buttonGroupSelectorApp;
