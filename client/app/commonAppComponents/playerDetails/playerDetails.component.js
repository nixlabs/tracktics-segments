class PlayerDetailsAppController{
  playerInfo = {
    firstName: "Igor",
    lastName: "Nikolov",
    position: "Player",
  };

  showJSONeditor = false;
  obj = {data:this.playerInfo, options: {mode: 'tree'}}
  newArrowData = null;
  /*@ngInject*/
  constructor() {
  }

  $onInit() {

  }
  btnOnClick(){
    this.showJSONeditor = !this.showJSONeditor;
    this.newArrowData = this.playerInfo;
    this.obj = {data:this.newArrowData, options: {mode: 'tree'}}
  }
  changeOptions () {
    this.obj.options.mode = this.obj.options.mode == 'tree' ? 'code' : 'tree';
  };
  showJSON(obj) {
    return angular.toJson(obj, true);
  };
  btnSave(){
    this.playerInfo = this.obj.data;

  }
}

let PlayerDetailsApp = {
    bindings: {},
    template: require('./playerDetails.html'),
    controllerAs: 'vm',
    controller: PlayerDetailsAppController
};

export default PlayerDetailsApp
