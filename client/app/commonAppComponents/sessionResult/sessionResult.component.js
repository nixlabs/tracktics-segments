export class sessionResultAppController {

  SessionResult = {
    ownTeam: {
      name: "Chelsea",
      image: "http://www.iconarchive.com/download/i12608/giannis-zographos/british-football-club/Chelsea.ico",
      goals: "2"
    },
    opponentTeam: {
      name: "Arsenal",
      image: "http://icons.veryicon.com/ico/Sport/British%20Football%20Club/Arsenal.ico",
      goals: "1"
    }
  };

  showJSONeditor = false;
  obj = {data:this.SessionResult, options: {mode: 'tree'}}
  newSessionResultInfo = null;
  /*@ngInject*/
  constructor() {
  }

  $onInit() {

  }
  btnOnClick(){
    this.showJSONeditor = !this.showJSONeditor;
    this.newSessionResultInfo = this.SessionResult;
    this.obj = {data:this.newSessionResultInfo, options: {mode: 'tree'}}
  }
  changeOptions () {
    this.obj.options.mode = this.obj.options.mode == 'tree' ? 'code' : 'tree';
  };
  showJSON(obj) {
    return angular.toJson(obj, true);
  };
  btnSave(){
    this.SessionResult = this.obj.data;

  }
}

let sessionResultApp = {
  bindings: {
    data: '<'
  },
  template: require('./sessionResult.html'),
  controllerAs: 'vm',
  controller: sessionResultAppController
};

export default sessionResultApp;
