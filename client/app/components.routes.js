'use strict';

export default function routes($stateProvider) {
  'ngInject';

  $stateProvider

    .state('statinfosinglecomparison', {
      url: '/statinfosinglecomparison',
      template: '<stat-info-single-comparison-app></stat-info-single-comparison-app>'
    })
    .state('statinfodoublecomparison', {
      url: '/statinfodoublecomparison',
      template: '<stat-info-double-comparison-percentage-app></stat-info-double-comparison-percentage-app>'
    })
    .state('shapesinglestat', {
      url: '/shapesinglestat',
      template: '<shape-single-stat-app></shape-single-stat-app>'
    })
    .state('sessionresult', {
      url: '/sessionresult',
      template: '<session-result-app></session-result-app>'
    })
    .state('sessionmetadata', {
      url: '/sessionmetadata',
      template: '<session-meta-data-app></session-meta-data-app>'
    })
    .state('arrow', {
      url: '/arrow',
      template: '<arrow-app></arrow-app>'
    })
    .state('statinfosinglecomparisonpercentage', {
      url: '/statinfosinglecomparisonpercentage',
      template: '<stat-info-single-comparison-percentage-app></stat-info-single-comparison-percentage-app>'
    })
    .state('buttongroupselector', {
      url: '/buttongroupselector',
      template: '<button-group-selector-app></button-group-selector-app>'
    })
    .state('editableinput', {
      url: '/editableinput',
      template: '<editable-input-app></editable-input-app>'
    })
    .state('editableemailinput', {
      url: '/editableemailinput',
      template: '<editable-email-input-app></editable-email-input-app>'
    })
    .state('periodselector', {
      url: '/periodselector',
      template: '<period-selector-app></period-selector-app>'
    })
    .state('statinfodonut', {
      url: '/statinfodonut',
      template: '<stat-info-donut-app></stat-info-donut-app>'
    })
    .state('playerdetails', {
      url: '/playerdetails',
      template: '<player-details-app></player-details-app>'
    })
    .state('pitchzone', {
      url: '/pitchzone',
      template: '<pitch-zone-app></pitch-zone-app>'
    })
    .state('areaspline', {
      url: '/areaspline',
      template: '<area-spline-app></area-spline-app>'
    })
    .state('bargraph', {
      url: '/bargraph',
      template: '<bar-graph-app></bar-graph-app>'
    })
    .state('doubledatacomparison', {
      url: '/doubledatacomparison',
      template: '<double-data-comparison-app></double-data-comparison-app>'
    })
    .state('buttonicon', {
      url: '/buttonicon',
      template: '<button-icon-app></button-icon-app>'
    })

    .state('buttons', {
      url: '/buttontheme',
      templateUrl: '../../theme/buttons.html'
    })
    .state('colors', {
      url: '/colorstheme',
      templateUrl: '../../theme/colors.html'
    })
    .state('images', {
      url: '/imagestheme',
      templateUrl: '../../theme/images.html'
    })
    .state('panels', {
      url: '/panelstheme',
      templateUrl: '../../theme/panels.html'
    })
    .state('sidebar', {
      url: '/sidebartheme',
      templateUrl: '../../theme/sidebar.html'
    })
    .state('verticalpanel', {
      url: '/vertical-panel',
      templateUrl: '../../theme/vertical-panel.html'
    })
    .state('trackticslogin', {
      url: '/login',
      templateUrl: '../../theme/tracktics-login.html'
    })
    .state('arrowsTheme', {
      url: '/arrowsTheme',
      templateUrl: '../../theme/arrows-theme.html'
    })
    .state('SessionDetailsTop', {
      url: '/SessionDetailsTop',
      templateUrl: '../../theme/session-details-top.html'
    })
    .state('PlayerDevelopmentTop', {
      url: '/PlayerDevelopmentTop',
      templateUrl: '../../theme/player-development-top.html'
    })
    .state('DistanceCoveredHS', {
      url: '/DistanceCoveredHighSpeed',
      templateUrl: '../../theme/distance-covered-high-speed.html'
    })
    .state('TeamSessionOverview', {
      url: '/TeamSessionOverview',
      templateUrl: '../../theme/team-session-overview.html'
    })
    .state('SessionGameTop', {
      url: '/SessionGameTop',
      templateUrl: '../../theme/session-game-top.html'
    })
    .state('PlayerDevelopmentTopNew', {
      url: '/PlayerDevelopmentTopNew',
      templateUrl: '../../theme/player-development-top-new.html'
    });

}
