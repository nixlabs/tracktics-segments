'use strict';
import angular from 'angular';
import ngCookies from 'angular-cookies';
import ngResource from 'angular-resource';
import ngSanitize from 'angular-sanitize';
import translate from 'angular-translate';
import translateStatic from 'angular-translate-loader-static-files';
import translateInterpolationFormat from 'angular-translate-interpolation-messageformat';
import routing from './components.routes';
import uiRouter from 'angular-ui-router';
import uiBootstrap from 'angular-ui-bootstrap';
import 'angular-chart.js'
import 'chart.js'
import service from '../services/services.module'
import common from '../common/common.module'

// import ngMessages from 'angular-messages';
import 'angular-jsoneditor';

import Highcharts from 'highcharts/highstock';
window.Highcharts = Highcharts; //this line did the magic
import 'highcharts-ng';

/*import 'heatmap.js';
import 'heatmap.js/plugins/angular-heatmap/angular-heatmap'*/

import 'jquery';

import commonAppModule from './commonAppComponents/commonApp.module'

import {
  routeConfig,appConfig
} from './app.config';

import main from './main/main.component';
import playerSession from './playerSession/playerSession.component';
import constants from './app.constants';

import './app.scss';

angular.module('trackticsSegmentsApp', [ngCookies, ngResource, ngSanitize, uiRouter, uiBootstrap,
  main, constants, translate, translateStatic, translateInterpolationFormat, playerSession,commonAppModule, 'tracktics-segments','angular-jsoneditor','highcharts-ng','chart.js',service,common
])
  .config(routeConfig)
  .config(routing)
  .config(appConfig);


angular.element(document)
  .ready(() => {
    angular.bootstrap(document, ['trackticsSegmentsApp'], {
      strictDi: true
    });
  });
