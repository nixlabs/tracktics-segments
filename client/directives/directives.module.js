import ttMovePill from './views/ttMovePill';
import ttNoEmptySpace from './views/ttNoEmptySpace';
import ttOnEnter from './views/ttOnEnter';
import ttEqualsTo from './views/ttEqualsTo';
import ttRangeInput from './views/ttRangeInput';
import ttInputLength from './views/ttInputLength';

export default angular.module('directives', [])
    .directive('ttMovePill', ttMovePill)
    .directive('ttNoEmptySpace', ttNoEmptySpace)
    .directive('ttOnEnter', ttOnEnter)
    .directive('ttEqualsTo', ttEqualsTo)
    .directive('ttRangeInput', ttRangeInput)
    .directive('ttInputLength', ttInputLength)
    .name;
