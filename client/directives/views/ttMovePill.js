const ttMovePill = (MovePillFactory, DOMElementUtils, StringUtils) => {
    'ngInject';
    return {
        restrict: 'A',
        link: function(scope, element, attrs){

            let _moveTo = (el) => {
                //let margin = parseInt(StringUtils.substring('px', DOMElementUtils.getStyle(el, 'margin-right')));
                element[0].style.transition = 'transform 0.3s ease-out, width 0.3s ease-out';
                element[0].style.WebkitTransition = 'transform 0.3s ease-out';
                element[0].style.transform = 'translate3d(' + (el.offsetLeft) + 'px, 0, 0)';
                element[0].style.WebkitTransform = 'translate3d(' + (el.offsetLeft) + 'px, 0, 0)';
                element[0].style.width = (el.offsetWidth) + 'px';
            };

            let movePillSub = MovePillFactory.element$.subscribe((value) => _moveTo(value));

            scope.$on('$destroy', () => {
                movePillSub.dispose();
            });
        }
    }
};

export default ttMovePill;