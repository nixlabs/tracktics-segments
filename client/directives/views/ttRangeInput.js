const ttRangeInput = (MessagesBus) => {
    'ngInject'

    return {
        restrict: 'A',
        link: (scope, element, attrs) => {

            function _isInRange(value, range){
                return value >= range.min && value <= range.max;
            }

            angular.element(element).on("keyup", (event) => {
                if (parseInt(event.target.value) > parseInt(attrs.max)) event.target.value = attrs.max;
                if (parseInt(event.target.value) < parseInt(attrs.min)) event.preventDefault;
            });
        }
    }
};

export default ttRangeInput;