const ttNoEmptySpace = ($timeout) => {
    'ngInject';
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {

            element.bind('keydown', (e) => {
                var charCode = (e.which) ? e.which : event.keyCode;
                if (charCode === 32)
                    return false;
                return true;
            });

            scope.$on('$destroy', () => {
                element.unbind('keydown');
            });
        }
    }
};

export default ttNoEmptySpace;