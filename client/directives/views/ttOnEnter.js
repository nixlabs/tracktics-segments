const ttOnEnter = (MessagesBus) => {
    'ngInject';
    return {
        restrict: 'A',
        replace: false,
        link(scope, element, attrs) {
            /* jshint unused:false */
            /* eslint "no-unused-vars": [2, {"args": "none"}] */

            let enterKeyPressedStream = Rx.Observable.fromEvent(element[0], 'keypress').filter((event) => event.keyCode == 13);
            let textEnteredStream = Rx.Observable.fromEvent(element[0], 'keypress').map((event) => event.target.value);

            let mergedUntilStream = textEnteredStream.takeUntil(enterKeyPressedStream);

            let text = '';
            let onNext = (t) => { text = t; };
            let onError = (e) => {  };
            let onComplete = () => {
                MessagesBus.sendMessage(text);
                mergedUntilStream.subscribe(onNext, onError, onComplete);
            };

            mergedUntilStream.subscribe(onNext, onError, onComplete);

        }
    };
};

export default ttOnEnter;