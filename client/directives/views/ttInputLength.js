const ttInputLength = () => {
    'ngInject'

    return {
        restrict: 'A',
        link: (scope, element, attrs) => {

            let limit = 3;

            angular.element(element).on("keypress", (event) => {
                if (event.target.value.length === limit) event.preventDefault();
            });
        }
    }
};

export default ttInputLength;