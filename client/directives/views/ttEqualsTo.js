const ttEqualsTo = () => {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: (scope, element, attrs, ngModel) =>{
            scope.$watch(attrs.ttEqualsTo, () => {
                ngModel.$validate();
            });

            ngModel.$validators.isEqualTo = (value) => {
                var equal = scope.$eval(attrs.ttEqualsTo);
                if (!equal || !value) {
                    return false;
                }
                return (value === equal);
            };
        }
    }
};

export default ttEqualsTo;