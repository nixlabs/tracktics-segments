import angular from 'angular';
import Utils from './util/util.module';


export default angular.module('services', [Utils])
    .name;
