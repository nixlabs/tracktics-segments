import _ from 'lodash';

export function StringUtilsService() {
    return {
        contains(obj, str) {
            //takes a key/value of object
            if (JSON.stringify(obj).indexOf(JSON.stringify(str)) > 0) {
                return true;
            } else {
                return false;
            }
        },

        trim(str) {
            return _.trim(str);
        },

        isBlank(str) {
            return !str || /^\s*$/.test(str);
        },

        removeWhiteSpace(string) {
            return string.replace(" ", "");
        },

        substring(needle, haystack) {
            return haystack.substring(0, haystack.indexOf(needle));
        },

        strContainsStr(str1, str2) {
            return str1.indexOf(str2) > -1;
        },

        capitalizeFirstLetter(str) {
            return str.charAt(0).toUpperCase() + str.slice(1);
        },

        truncate(number) {
            return Math.round(number * 10) / 10;
        }
    }
}
