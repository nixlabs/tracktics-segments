'use strict';
import moment from 'moment';

export function TimeUtilsService() {
    let daysShort = ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'];
    let monthsShort = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    let unixtimestampStart = '1970-01-01 00:00:00';

    return {
        secondsToTime(seconds){
            if ((seconds / 60) < 1) {
                return (seconds).toFixed(0) + " sec";
            } else {
                return (seconds / 60).toFixed(0) + " min";
            }
        },

        milisecondsToMinutes(ms){
            return Math.round((ms / 1000) / 60);
        },

        milisecondsToSeconds(ms){
            return Math.round(ms / 1000);
        },
        
        minutesToSeconds(m){
            return Math.round((m * 60));
        },
        
        minutesToMiliseconds(m){
            return Math.round((m * 60) * 1000);
        },
        
        secondsToMiliseconds(s){
            return Math.round(s * 1000);
        },
        
        secondsToMinutes(s){
            return Math.round(s / 60);
        },
        
        dateToUnixTimestamp(date){
            return moment.utc(date, "YYYY-MM-DD HH:mm:ss").unix();
        },
        
        unixTimestampToDate(timestamp, format){
            let timeFormat = format ? format : "YYYY-MM-DD HH:mm:ss";
            return moment.unix(timestamp).format(timeFormat);
        },
        
        setDateInFormat(adate, format, separators = ['. ', ' ']){
            let date = moment(adate);
            if (typeof(format) === 'undefined') {
                format = {day: 'D', month: 'MMMM', year: 'YYYY'};
            }
            return date.format(format.day) + separators[0] + date.format(format.month) + separators[1] + date.format(format.year);
        },
        
        setTimeInFormat(atime, format, separators = [':']){
            let time = moment(atime);
            if (typeof(format) === 'undefined') {
                format = {hours: 'HH', minutes: 'mm'};
            }
            return time.format(format.hours) + separators[0] + time.format(format.minutes);
        },

        getTimeFromDate(date){
            return moment(date).format("HH:mm");
        },

        getDateFromDateInFormat(date, format){
            return moment(date).format(format);
        },
        
        dateToStringDate(date) {
            date = date.replace(/-/g, '/');
            var newDate = new Date(date);
            var tmpMonth = newDate.getMonth();
            var tmpDay = newDate.getDay();
            var tmpDate = newDate.getDate();
            var tmpYear = newDate.getFullYear();

            return daysShort[tmpDay] + ', ' + tmpDate + '.' + monthsShort[tmpMonth] + '.' + tmpYear.toString().substring(2);
        },
        
        dateToStringTime(sDate, eDate) {
            sDate = sDate.replace(/-/g, '/');
            eDate = eDate.replace(/-/g, '/');
            var tmpStart = new Date(sDate);
            var tmpEnd = new Date(eDate);

            var tmpHourS = tmpStart.getHours();
            var tmpMinuteS = tmpStart.getMinutes();
            if (tmpHourS < 10)
                tmpHourS = "0" + tmpHourS;
            if (tmpMinuteS < 10)
                tmpMinuteS = "0" + tmpMinuteS;

            var tmpHourE = tmpEnd.getHours();
            var tmpMinuteE = tmpEnd.getMinutes();
            if (tmpHourE < 10)
                tmpHourE = "0" + tmpHourE;
            if (tmpMinuteE < 10)
                tmpMinuteE = "0" + tmpMinuteE;


            return tmpHourS + ":" + tmpMinuteS + " - " + tmpHourE + ":" + tmpMinuteE;
        },
        
        difference(tDate, fDate, criteria = 'months'){
            return Math.abs(moment([tDate.getFullYear(), tDate.getMonth(), tDate.getDate()])
                .diff(moment([fDate.getFullYear(), fDate.getMonth(), fDate.getDate()]), criteria, true));
        },
        
        getStructuredTimeDifference(timeInMonths, fDate){
            if (timeInMonths > 12) {
                return {
                    type: 'years',
                    duration: Math.round(timeInMonths / 12)
                };
            }
            else if (timeInMonths <= 12 && timeInMonths > 1) {
                return {
                    type: 'months',
                    duration: timeInMonths
                };
            }
            else {
                return {
                    type: 'days',
                    duration: difference(new Date, fDate, 'days')
                };
            }
        },
        
        getNameOfDay(date){
            return moment(date).format('dddd');
        },
        
        getMonthSeason(month){
            let winter = 'sep,september,oct,october,nov,november,9,10,11,dec,december,jan,january,feb,february,12,1,2,';
            //let spring = 'mar,march,apr,april,may,3,4,5,';
            let summer = 'mar,march,apr,april,may,3,4,5,jun,june,jul,july,aug,august,6,7,8,';
            //let autumn = 'sep,september,oct,october,nov,november,9,10,11,';

            if (winter.indexOf(month) != -1) {
                return 'winter';
            } else if (summer.indexOf(month) != -1) {
                return 'summer';
            }
        },
        
        convertUTCTimeToTimezone(utcTime){
            let time = moment.utc(utcTime).toDate();
            return moment(time).format('YYYY-MM-DD HH:mm:ss');
        },
        
        isDateUnixTimestampStart(date){
            return moment.utc(date, "YYYY-MM-DD HH:mm:ss").unix() === moment.utc(unixtimestampStart, "YYYY-MM-DD HH:mm:ss").unix();
        },
        
        getDateDifference(refDate){

            let today = new Date();
            let fDate = moment(refDate).toDate();

            return getStructuredTimeDifference(TimeUtilsBase.difference(today, fDate), fDate);
        }
    };
}
