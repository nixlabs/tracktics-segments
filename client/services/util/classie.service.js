'use strict';

export function Classie(){
    'ngInject'
    return {
        addClass(element, name){
            element.classList ? element.classList.add(name) : element.className += ' ' + name;
        },

        removeClass(element, name){
            var reg = new RegExp('(\\s|^)'+name+'(\\s|$)');
            element.className = element.className.replace(reg,' ');
        },

        removeSVGClass(element, name){
            var reg = new RegExp('(\\s|^)'+name+'(\\s|$)');
            element.className.baseVal = element.className.baseVal.replace(reg,' ');
        },

        hasClass(element, name){

            if(element.classList){
                return element.classList.contains(name);
            }else{
                var reg = new RegExp('(\\s|^)'+name+'(\\s|$)');
                return reg.test(element.className);
            }
        },

        toggleClass(element, name){
            /*let classes = element.className;
             let regex = new RegExp(name);
             let hasClass = regex.test(classes);

             (hasClass) ? Classie.removeClass(element, name) : Classie.addClass(element, name);*/
            element.classList.toggle(name);
        }

    };
};

