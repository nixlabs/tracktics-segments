'use strict';

import angular from 'angular';
import { UtilService } from './util.service';
import { TimeUtilsService } from './time.service';
import { StringUtilsService } from './string.service';
import { Classie } from './classie.service';

export default angular.module('trackticsWebappV2App.util', [])
  .factory('Util', UtilService)
  .factory('TimeUtils', TimeUtilsService)
  .factory('StringUtils', StringUtilsService)
  .factory('Classie', Classie)
  .name;
