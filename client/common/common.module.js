
import Abbr from './filters/abbr.filter';
import FixedLength from './filters/fixedLength.filter';

export default angular.module('common', [])
    .filter('abbr', Abbr)
    .filter('fixedLength', FixedLength)
    .name;
