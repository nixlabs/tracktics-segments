export default () => {
  return (value = '', length = 2) => {
    if (value === '') return value;
    value = parseFloat(value);
    return parseFloat(value.toFixed(length));
  }
}
