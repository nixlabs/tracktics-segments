export default () => {
    return (text = '', doAbbr = false) => {
        return (doAbbr && text.length > 4) ? text.substring(0, 4) + '.' : text;
    }
}
