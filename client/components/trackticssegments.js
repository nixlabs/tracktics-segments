import CommonComponents from './common/commonComponents.module';

export default angular.module('tracktics-segments', [CommonComponents])
  .name;
