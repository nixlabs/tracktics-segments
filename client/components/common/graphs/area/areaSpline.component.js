import _ from 'lodash';
class AreaSplineController {
  /*@ngInject*/
  constructor($element) {


    /*var gradient = ctx.createLinearGradient(0, 0, 0, 400);
     gradient.addColorStop(0, 'rgba(250,174,50,1)');
     gradient.addColorStop(1, 'rgba(250,174,50,0)');*/

    var ctx = $element[0].querySelector('.chart').getContext('2d');

    this.maingradient = ctx.createLinearGradient(0, 0, 0, 400);
    this.maingradient.addColorStop(0, 'rgba(255, 255, 255, 1)');
    this.maingradient.addColorStop(0.4, 'rgba(255, 255, 255, 0.2)');

    this.gradient = ctx.createLinearGradient(0, 0, 0, 400);
    this.gradient.addColorStop(0, 'rgba(40, 38, 51, 1 )');
    this.gradient.addColorStop(0.4, 'rgba(40, 38, 51, 0.5)');

    this.options = {
      maintainAspectRatio: false,
      responsive: true,
      fill: true,
      backgroundColor: '#fff',
      fillColor: this.maingradient,
      elements: {
        point: {
          radius: 0
        }
      },
      layout: {
        padding: 0
      },

      scales: {
        xAxes: [{
          ticks: {
            beginAtZero: true,
            fontColor: "#fff",
            maxRotation: 0,
          },
          gridLines: {
            display:false
          },
          afterBuildTicks: (chart) => {
            /*let length = chart.ticks.length;
             chart.ticks = [];
             chart.ticks.push(0);
             chart.ticks.push(length);*/
          }
        }],
        yAxes: [{
          display:false,
          gridLines: {
            display:false
          },
          ticks: {
            beginAtZero: true,
            display: false,
            reverse: false,
            min: 0,
            suggestedMin: 0
          },
        }]
      }
    };

    this.chart = new Chart(ctx, {
      type: 'line',
      options: this.options
    });
  }

  $onChanges(obj) {

    console.log(this.chartData.data[0]);
    if (Array.isArray(this.chartData.data[0])) {

      this.chart.data.labels = this.chartData.labels[0];
      let datasets = _.reduce(this.chartData.data, (prev, current, index) => {

        let gradient = (index % 2 === 0) ? this.gradient : this.maingradient;

        prev.push({
          data: current,
          backgroundColor: gradient,
          pointRadius: 0,
          spanGaps: true,
          showLine: true,
          borderWidth: 3,
          borderColor: '#fff'
        });

        return prev;

      }, []);

      this.chart.data.datasets = datasets;
      this.chart.update(0);
    }
    else {
      console.log(this.chartData);
      this.chart.data.labels = this.chartData.labels;
      this.chart.data.datasets = [{
        data: this.chartData.data,
        backgroundColor: this.maingradient,
        spanGaps: true,
        showLine: true,
        borderWidth: 3,
        borderColor: '#fff',
        pointRadius: 0
      }];
      this.chart.update(0);
    }
  }
}

let AreaSpline = {
  bindings: {
    chartId: '<',
    chartData: '<',
    onClick: '&?'
  },
  template: require('./areaSpline.html'),
  controllerAs: 'vm',
  controller: AreaSplineController
};

export default AreaSpline
