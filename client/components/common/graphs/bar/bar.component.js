class BarGraphController {
  /*@ngInject*/
  constructor() {
    this.options = {
      layout: {
        padding: 0
      },
      scales: {
        xAxes: [{
          display:false,
          gridLines: {
            display:false
          }
        }],
        yAxes: [{
          display: false,
          gridLines: {
            display:false
          },
          ticks: {
            beginAtZero:true
          }
        }]
      },
      legend: {
        display: false
      },
    }
  }


  _colorize(){
    return _.map(this.chartData.data, (data) => {
      if(this.chartColoring){
        if(data > 0) return '#00ff00';
        if(data < 0) return '#ff0000';
      }
      return '#fff';
    })
  }

  $onChanges() {
    this.datasetOverride = {
      backgroundColor: this._colorize(),
      hoverBackgroundColor: this._colorize(),
    };
  }
}

let BarGraph = {
  bindings: {
    chartId: '<',
    chartData: '<',
    chartColoring: '<'
  },
  template: require('./barGraph.html'),
  controllerAs: 'vm',
  controller: BarGraphController
};

export default BarGraph
