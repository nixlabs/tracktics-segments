export class ArrowController {
    /*@ngInject*/
    constructor() {
        
    }

    $onInit() {
    }
    
    $onChanges(){
        this.arrowClass = (this.data > 0) ? 'up' : 'down'
    }
}

let Arrow = {
    bindings: {
        data: '<',
        classes: '<?'
    },
    template: require('./arrow.html'),
    controllerAs: 'vm',
    controller: ArrowController
};

export default Arrow;
