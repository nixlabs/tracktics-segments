export class sessionMetaDataController {
  /*@ngInject*/
  constructor() {
  }

  $onInit() {
  }
}

let sessionMetaData = {
  bindings: {
    data: '<'
  },
  template: require('./sessionMetadata.html'),
  controllerAs: 'vm',
  controller: sessionMetaDataController
};

export default sessionMetaData;
