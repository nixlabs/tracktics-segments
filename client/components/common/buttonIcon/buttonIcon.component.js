class ButtonIconController{
   /*@ngInject*/
   constructor(){}
}

let ButtonIcon = {
    bindings: {
        icon: '<',
        text: '<',
        callback: '&'
    },
    template: require('./buttonIcon.html'),
    controllerAs: 'vm',
    controller: ButtonIconController
};

export default ButtonIcon