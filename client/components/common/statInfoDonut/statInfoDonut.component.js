class StatInfoDonutController {
  /*@ngInject*/
  constructor() {
  }

  $onChanges(obj) {
    if(this.class === 'large'){
      this.datasetOverride = {
        borderWidth: [0, 0],
        backgroundColor: ["#4A90E2", "#282633"],
        hoverBackgroundColor: ["#4A90E2", "#282633"],
      };
      this.options = {
        cutoutPercentage: 85
      }
    }
    else{
      this.datasetOverride = {
        borderWidth: [0, 0],
        backgroundColor: ["#9F9DAC", "#282633"],
        hoverBackgroundColor: ["#9F9DAC", "#282633"],
      };
      this.options = {
        cutoutPercentage: 65
      }
    }
  }
}

let StatInfoDonut = {
  bindings: {
    chartId: '<',
    chartData: '<',
    label: '<?',
    class: '<?'
  },
  template: require('./statInfoDonut.html'),
  controllerAs: 'vm',
  controller: StatInfoDonutController
};

export default StatInfoDonut
