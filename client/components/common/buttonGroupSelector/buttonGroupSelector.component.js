class ButtonGroupSelectorController {
    currentlySelected = 1;
    /*@ngInject*/
    constructor() {
    }

    $onInit() {
    }

    setActiveButton(obj) {

        for (let button of this.data.buttons) {
            if (button.selected) {
                button.selected = false;
            }
        }
        obj.selected = true;
        this.onButtonSelection({filter: obj.data});
    }

}

let ButtonGroupSelector = {
    bindings: {
        data: '<',
        onButtonSelection: '&'
    },
    template: require('./buttonGroupSelector.html'),
    controllerAs: 'vm',
    controller: ButtonGroupSelectorController
};

export default ButtonGroupSelector;
