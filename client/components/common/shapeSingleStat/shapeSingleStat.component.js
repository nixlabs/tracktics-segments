export class shapeSingleStatController {
  /*@ngInject*/
  constructor() {
  }

  $onInit() {
  }
}

let shapeSingleStat = {
  bindings: {
    data: '<'
  },
  template: require('./shapeSingleStat.html'),
  controllerAs: 'vm',
  controller: shapeSingleStatController
};

export default shapeSingleStat;
