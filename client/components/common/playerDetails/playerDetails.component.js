class PlayerDetailsController{
   /*@ngInject*/
   constructor(){}
}

let PlayerDetails = {
    bindings: {
        data: '<'
    },
    template: require('./playerDetails.html'),
    controllerAs: 'vm',
    controller: PlayerDetailsController
};

export default PlayerDetails