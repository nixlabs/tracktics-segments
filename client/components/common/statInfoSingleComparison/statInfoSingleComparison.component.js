class StatInfoSingleComparisonController {
    /*@ngInject*/
    constructor() {
    }

    $onInit() {
    }
}

let StatInfoSingleComparison = {
    bindings: {
        data: '<'
    },
    template: require('./statInfoSingleComparison.html'),
    controllerAs: 'vm',
    controller: StatInfoSingleComparisonController
};

export default StatInfoSingleComparison;
