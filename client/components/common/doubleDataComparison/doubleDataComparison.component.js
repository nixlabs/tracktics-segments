class DoubleDataComparisonController{

   /*@ngInject*/
   constructor(){
       this.metric = '%';
   }
}

let DoubleDataComparison = {
    bindings: {
        title: '<',
        subtitle: '<',
        primary: '<',
        secondary: '<',
        metric: '<?'
    },
    template: require('./doubleDataComparison.html'),
    controllerAs: 'vm',
    controller: DoubleDataComparisonController
};

export default DoubleDataComparison
