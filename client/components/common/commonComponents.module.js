import EditableInput from './editableInput/editableInput.component';
import PlayerDetails from './playerDetails/playerDetails.component';
import StatInfoSingleComparison from './statInfoSingleComparison/statInfoSingleComparison.component';
import Arrow from './arrow/arrow.component';
import ShapeSingleStat from './shapeSingleStat/shapeSingleStat.component';
import SessionResult from './sessionResult/sessionResult.component';
import SessionMetadata from './sessionMetadata/sessionMetadata.component';
import PeriodSelector from './periodSelector/periodSelector.component';
import ttPeriodSelector from './periodSelector/ttPeriodSelector.directive';
import StatInfoDonut from './statInfoDonut/statInfoDonut.component';
import ButtonGroupSelector from './buttonGroupSelector/buttonGroupSelector.component';
import ButtonIcon from './buttonIcon/buttonIcon.component';
import AreaSpline from './graphs/area/areaSpline.component';
import BarGraph from './graphs/bar/bar.component';
import Heatmap from './heatmap/heatMap.component';
import ttHeatmap from './heatmap/ttHeatmap.directive';
import ttFlexiblePitch from './heatmap/ttFlexiblePitch.directive';
import PitchZone from './pitchZone/pitchZone.component';
import DoubleDataComparison from './doubleDataComparison/doubleDataComparison.component';
import EditableEmailInput from './editableEmailInput/editableEmailInput.component'

export default angular.module('trackticsWebappV2App.commonComponents', [])
    .component('editableInput', EditableInput)
    .component('playerDetails', PlayerDetails)
    .component('statInfoSingleComparison', StatInfoSingleComparison)
    .component('arrow', Arrow)
    .component('shapeSingleStat', ShapeSingleStat)
    .component('sessionResult', SessionResult)
    .component('sessionMetadata', SessionMetadata)
    .component('periodSelector', PeriodSelector)
    .directive('ttPeriodSelector', ttPeriodSelector)
    .component('statInfoDonut', StatInfoDonut)
    .component('buttonGroupSelector', ButtonGroupSelector)
    .component('buttonIcon', ButtonIcon)
    .component('areaSpline', AreaSpline)
    .component('barGraph', BarGraph)
    .component('heatmap', Heatmap)
    .directive('ttHeatmap', ttHeatmap)
    .directive('ttFlexiblePitch', ttFlexiblePitch)
    .component('pitchZone', PitchZone)
    .component('doubleDataComparison', DoubleDataComparison)
    .component('editableEmailInput', EditableEmailInput)
    .name;
