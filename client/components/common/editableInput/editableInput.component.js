/**
 * Created by Igor on 08-Mar-17.
 */

export class editableInputController {

    editButton = false;
    isArray = false;
    newData = [];
    newStringData = null;


    /*@ngInject*/
    constructor() {
      console.log(this.data);
    }

    $onInit() {
        this.isArray = Array.isArray(this.data);
      console.log(this.data);
    }

    editBtn() {
        if (this.isArray) {
            for (let i = 0; i < this.data.length; i++) {
                this.newData[i] = this.data[i].name;
            }
        }
        else {
            this.newStringData = this.data;
        }
        return this.editButton = !this.editButton;
    }

    cancel() {
        this.editButton = !this.editButton;
    }

    save() {
        if (this.isArray) {
            for (let i = 0; i < this.data.length; i++) {
                this.data[i].name = this.newData[i];
                console.log(this.data[i].name);
            }
        }
        else {
            this.data = this.newStringData;
        }
      this.callback({data: this.data});

      this.editButton = !this.editButton;


    }
}

let editableInput = {
    bindings: {
        data: "<",
        label: "<",
        callback: '&'
    },
    template: require('./editableInput.html'),
    controllerAs: 'vm',
    controller: editableInputController
};

export default editableInput;
