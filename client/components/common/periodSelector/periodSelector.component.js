class PeriodSelectorController {
    /*@ngInject*/
    constructor() {
    }
}

let PeriodSelector = {
    bindings: {
        onChange: '&',
        onRemove: '&',
        id: '<',
        max: '<',
        from: '<',
        to: '<',
        fromMin: '<',
        toMax: '<'
    },
    template: require('./periodSelector.html'),
    controllerAs: 'vm',
    controller: PeriodSelectorController
};

export default PeriodSelector