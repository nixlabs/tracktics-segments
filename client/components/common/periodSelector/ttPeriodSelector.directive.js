'use strict';
import _ from 'lodash';

const ttPeriodSelector = (Classie, StringUtils) => {
    'ngInject'
    
    return {
        restrict: 'A',
        require: '^periodSelector',
        link: (scope, element, attrs, controller) => {
            let el = element[0].querySelector('input');
            let close = element[0].querySelector('span.close');
            let bar = null;

            function _getSiblings(element, type) {
                var arraySib = [];
                if (type == 'prev') {
                    while (element = element.previousSibling) {
                        arraySib.push(element);
                    }
                } else if (type == 'next') {
                    while (element = element.nextSibling) {
                        arraySib.push(element);
                    }
                }
                return arraySib;
            }
            
            function _setSelected(obj){
                let minElement = element[0].querySelector('span.js-grid-text-' + obj.from);
                let maxElement = element[0].querySelector('span.js-grid-text-' + obj.to);

                let middleElements = Array.prototype.slice.call(element[0].querySelectorAll('span.irs-grid-pol'));
                let allPrevElements = _getSiblings(minElement, 'prev');
                let allNextElements = _getSiblings(maxElement, 'next');

                _.each(middleElements.slice(obj.from, obj.to), (element) => {
                    if (Classie.hasClass(element, 'not-selected')) {
                        Classie.removeClass(element, 'not-selected');
                    }
                });

                _.each(allPrevElements, (element) => {
                    if (!Classie.hasClass(element, 'not-selected')) {
                        Classie.addClass(element, 'not-selected');
                    }
                });

                _.each(allNextElements, (element) => {
                    if (!Classie.hasClass(element, 'not-selected')) {
                        Classie.addClass(element, 'not-selected');
                    }
                });
            }

            function _calculateStyle(element, bar){
                console.log(parseInt(StringUtils.substring('%', bar.style.left)) + parseInt(StringUtils.substring('%', bar.style.width)) / 2 - element.offsetWidth / 2);
                return parseInt(StringUtils.substring('%', bar.style.left)) + parseInt(StringUtils.substring('%', bar.style.width)) / 2;// - element.offsetWidth / 2;
            }

            angular.element(el).ionRangeSlider({
                type: 'double',
                grid: true,
                grid_num: attrs.max,
                drag_interval: true,
                hide_min_max: true,
                hide_from_to: true,
                min_interval: 10,
                step: 1,
                min: 0,
                max: attrs.max,
                from: attrs.from,
                to: attrs.to,
                from_min: attrs.fromMin,
                to_max: attrs.toMax,
                onStart: (obj) => {
                    bar = element[0].querySelector('.irs-bar');
                    close.style.left = _calculateStyle(close, bar) + '%';
                    _setSelected(obj);
                },
                onChange: (obj) => {
                    _.throttle(() => {
                        controller.onChange({selector: {id: parseInt(attrs.id), from: obj.from, to: obj.to}})
                    }, 100)();
                    _setSelected(obj);
                    bar = element[0].querySelector('.irs-bar');
                    close.style.left = _calculateStyle(close, bar) + '%';
                },
                onFinish: (obj) => {

                },
                onUpdate: (obj) => {

                }
            });

            let slider = angular.element(el).data('ionRangeSlider');

            function updateSlider(){
                slider.update({
                    to_max: attrs.toMax,
                    from_min: attrs.fromMin
                });
            }

            attrs.$observe('fromMin', (newValue, oldValue) => {
                if(newValue !== oldValue){
                    updateSlider();
                }
            });

            attrs.$observe('toMax', (newValue, oldValue) => {
                if(newValue !== oldValue){
                    updateSlider();
                }
            });
        }
    }
};

export default ttPeriodSelector;