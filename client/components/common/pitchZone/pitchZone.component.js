/**
 * Created by Igor on 08-Mar-17.
 */

export class pitchZoneController {

    /*@ngInject*/
    constructor() {
    }

    $onInit() {

    }
}

let pitchZone = {
    bindings: {
      type: "<"
    },
    template: require('./pitchZone.html'),
    controllerAs: 'vm',
    controller: pitchZoneController
};

export default pitchZone;
