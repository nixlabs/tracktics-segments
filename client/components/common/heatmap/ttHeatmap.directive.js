var h337 = require('heatmap.js');

/**
 * Created by NixDev on 3/23/2017.
 */
const ttHeatmap = () => {
  'ngInject'

  return {
    restrict: 'A',
    require: '^heatmap',
    link: (scope, element, attrs, controller) => {

      let heatmap = h337.create({
        container: element[0],
        gradient: {
          // enter n keys between 0 and 1 here
          // for gradient color customization
          '.1': 'rgba(22, 25, 51, 1)',
          '.3': 'rgba(21, 208, 190, 1)',
          '.5': 'rgba(25, 210, 19, 1)',
          '.7': 'rgba(255, 221, 0, 1)',
          '.9': 'rgba(255, 7, 0, 1)',
          '.95': 'rgba(255, 255, 255, 1)'
        },
      });

      heatmap.setData({
        max: 5,
        data: [{ x: 10, y: 15, value: 5},{ x: 10, y: 15, value: 5},{ x:121, y: 5, value: 80},{ x: 13, y: 55, value: 5},{ x: 12, y: 15, value: 5}]
      });

      function _setData(data){
        heatmap.setData({
          min: 0,
          max: 100,
          data: data
        });
        element[0].querySelector('canvas').style.height = attrs.pitchHeight + 'px';
      }

      scope.$watch(() => {
        return controller.data.data
      }, (newValue, oldValue) => {
        if(newValue !== oldValue){
          _setData(newValue);
        }
      });
    }
  }
};

export default ttHeatmap;
