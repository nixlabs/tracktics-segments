const ttFlexiblePitch = () => {
    'ngInject'
    
    return {
        restrict: 'A',
        link: function(scope, element, attrs){

            let _setPitchSize = (canvasSize) => {
                element[0].style.height = canvasSize + 'px';
            };

            attrs.$observe('pitchHeight', (newValue, oldValue) => {
                if(newValue !== oldValue){
                    _setPitchSize(newValue);
                }
            });
        }
    }
};

export default ttFlexiblePitch;