
export class heatMapController {
    heatmap = {};
    /*@ngInject*/
    constructor() {
        this.config = {
            gradient: {
                // enter n keys between 0 and 1 here
                // for gradient color customization
                '.1': 'rgba(22, 25, 51, 1)',
                '.3': 'rgba(21, 208, 190, 1)',
                '.5': 'rgba(25, 210, 19, 1)',
                '.7': 'rgba(255, 221, 0, 1)',
                '.9': 'rgba(255, 7, 0, 1)',
                '.95': 'rgba(255, 255, 255, 1)'
            },
        };
    }

    $onChanges() {
    }


    


}

let heatMap = {
    bindings: {
        data: '<'
    },
    template: require('./heatMap.html'),
    controllerAs: 'vm',
    controller: heatMapController
};

export default heatMap;
