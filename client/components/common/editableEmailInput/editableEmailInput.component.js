/**
 * Created by Igor on 08-Mar-17.
 */

export class editableEmailInputController {

    editButton = false;
    isArray = false;
    newData = [];
    newStringData = null;


    /*@ngInject*/
    constructor() {
    }

    $onInit() {
        this.isArray = Array.isArray(this.data);
    }

    editBtn() {
            this.newStringData = this.data;
        return this.editButton = !this.editButton;
    }

    cancel() {
        this.editButton = !this.editButton;
    }


    save() {
         if(this.validateEmail(this.newStringData)){
           console.log("This Email is valid");
            this.data = this.newStringData;
          }
          else{
            console.log(this.validateMsg);
         }

        this.callback({data: this.data});

        this.editButton = !this.editButton;
    }

  validateEmail(email) {
  let re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
  }
}

let editableEmailInput = {
    bindings: {
        data: '<',
        validate: '&',
        validateMsg: '<',
        callback: '&'
    },
    template: require('./editableEmailInput.html'),
    controllerAs: 'vm',
    controller: editableEmailInputController
};

export default editableEmailInput;
