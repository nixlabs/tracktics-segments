export class sessionResultController {
  /*@ngInject*/
  constructor() {
  }

  $onInit() {
  }
}

let sessionResult = {
  bindings: {
    data: '<'
  },
  template: require('./sessionResult.html'),
  controllerAs: 'vm',
  controller: sessionResultController
};

export default sessionResult;
